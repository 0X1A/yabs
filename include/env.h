// Copyright (c) 2013-2014, Alberto Corona <alberto@0x1a.us>
// All rights reserved. This file is part of yabs, distributed under the BSD
// 3-Clause license. For full terms please see the LICENSE file.

#ifndef _ENV_H
#define _ENV_H

#include <stdlib.h>

class Env
{
public:
	Env();
	~Env();
};

#endif
