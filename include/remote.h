// Copyright (c) 2013-2014, Alberto Corona <alberto@0x1a.us>
// All rights reserved. This file is part of yabs, distributed under the BSD
// 3-Clause license. For full terms please see the LICENSE file.

#ifndef _REMOTE_H
#define _REMOTE_H

#include <string.h>
#include <sys/types.h>
#include <iostream>
#include <fstream>

class Remote
{
public:
	Remote();
	~Remote();
};

#endif
